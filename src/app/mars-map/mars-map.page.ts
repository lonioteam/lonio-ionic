import { Component, OnInit, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-mars-map',
  templateUrl: './mars-map.page.html',
  styleUrls: ['./mars-map.page.scss'],
})
export class MarsMapPage implements OnInit {

  dragItem;
  container;
  spot;

  currentX;
  currentY;
  initialX;
  initialY;
  xOffset = 0;
  yOffset = 0;


  w = window.innerWidth;

  constructor(private elementRef: ElementRef, private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    //this.elementRef.nativeElement.querySelector("#marsMap").addEventListener("touchmove", this.onDrag.bind(this));

    this.dragItem = this.elementRef.nativeElement.querySelector("#item");
    this.container = this.elementRef.nativeElement.querySelector("#container");
    this.spot = this.elementRef.nativeElement.querySelector("#spot");
    
    if (this.spot) {
      this.spot.style.left = 300 + "px";
      this.spot.style.top = 300 + "px";
    }
    console.log(this.spot);

    this.container.addEventListener("touchstart", this.dragStart.bind(this), false);
    this.container.addEventListener("touchend", this.dragEnd.bind(this), false);

    this.elementRef.nativeElement.querySelector("#container").addEventListener("touchmove", this.touchStart.bind(this));
    //this.elementRef.nativeElement.querySelector("#marsMap").addEventListener("touchend", this.touchEnd.bind(this));
  }

  closeMap() {
    this.modalCtrl.dismiss();
  }

  dragStart(e) {
    if (e.type === "touchstart") {
      this.initialX = e.touches[0].clientX - this.xOffset;
      this.initialY = e.touches[0].clientY - this.yOffset;
    } else {
      this.initialX = e.clientX - this.xOffset;
      this.initialY = e.clientY - this.yOffset;
    }
  }

  dragEnd(e) {
    this.initialX = this.currentX;
    this.initialY = this.currentY;
  }

  touchStart(e) {
      
        e.preventDefault();
      
        if (e.type === "touchmove") {
          this.currentX = e.touches[0].clientX - this.initialX;
          this.currentY = e.touches[0].clientY - this.initialY;
        } else {
          this.currentX = e.clientX - this.initialX;
          this.currentY = e.clientY - this.initialY;
        }

        this.xOffset = this.currentX;
        this.yOffset = this.currentY;

        console.log(this.w);
        console.log(this.currentX);
        console.log(this.initialX);

        if (this.currentX > 0) this.currentX = 0;
        if (this.currentX < this.w - 1000) this.currentX = this.w - 1000;


        this.setTranslate(this.currentX, 0, this.dragItem);
  }

  setTranslate(xPos, yPos, el) {
    el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
  }

  touchEnd(event) {
    console.log(event);
  }

  onDrag(event) {
    console.log(event);

    //let x2 = event.targetTouches[0].pageX - event.target.clientX;
    //let y = event.targetTouches[0].pageY - rect.top;
    //console.log(x2);


    //let touch = event.touches[0] || event.changedTouches[0];
    //let realTarget = document.elementFromPoint(touch.clientX, touch.clientY);
    //let x2 = touch.clientX-touch.screenX;
    //event.offsetY = touch.clientY-realTarget.getBoundingClientRect().y;

    //this.elementRef.nativeElement.querySelector("#marsMap").style.backgroundPositionX += x + "px";
    //this.elementRef.nativeElement.querySelector("#marsImage").style.backgroundPositionX = -event.offsetX + "px";
  }

}
