import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MessagesPage } from './messages.page';
import { TypingAnimationDirective } from 'angular-typing-animation';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: MessagesPage }])
  ],
  declarations: [MessagesPage, TypingAnimationDirective]
})
export class MessagesPageModule {}
