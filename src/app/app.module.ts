import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';

import { TypingAnimationDirective } from 'angular-typing-animation'

import { IonicStorageModule } from '@ionic/storage';

import { Camera } from '@ionic-native/camera/ngx';

import { SplashPage } from './splash/splash.page';
import { SplashPageModule } from './splash/splash.module';
import { MarsMapPage } from './mars-map/mars-map.page';
import { MarsMapPageModule } from './mars-map/mars-map.module';

import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';

import { NativeAudio } from '@ionic-native/native-audio/ngx';

import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';

import { Vibration } from '@ionic-native/vibration/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [
    SplashPage,
    MarsMapPage
   ],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule,  IonicStorageModule.forRoot(), SplashPageModule, MarsMapPageModule],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    TextToSpeech,
    NativeAudio,
    DeviceFeedback,
    Vibration,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
