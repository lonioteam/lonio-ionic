import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'messages',
        children: [
          {
            path: '',
            loadChildren: '../messages/messages.module#MessagesPageModule'
          }
        ]
      },
      {
        path: 'maps',
        children: [
          {
            path: '',
            loadChildren: '../maps/maps.module#MapsPageModule'
          }
        ]
      },
      {
        path: 'suits',
        children: [
          {
            path: '',
            loadChildren: '../suits/suits.module#SuitsPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/messages',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/messages',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
