import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-suits',
  templateUrl: 'suits.page.html',
  styleUrls: ['suits.page.scss']
})
export class SuitsPage {

  scriptsLoaded = false;
  numOfLoadedScripts = 0;

  currentSuit = "rogerSuit";

  suitChanged(event) {
    this.currentSuit = event.detail.value;
  }

  clickedPart(part){
    console.log(part);
  }

  /* constructor() {
    this.loadScript("../../assets/js/three.min.js").then(() => {
      this.loadScript("../../assets/js/OBJLoader.js").then(() => {
        this.loadScript("../../assets/js/model-element.min.js").then(() => {
          console.log("kocka kockakakkakakaka!");
          this.scriptsLoaded = true;
        });
      });
    });

    this.scriptsLoaded = true;
  } */

}



