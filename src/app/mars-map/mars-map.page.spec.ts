import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarsMapPage } from './mars-map.page';

describe('MarsMapPage', () => {
  let component: MarsMapPage;
  let fixture: ComponentFixture<MarsMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarsMapPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarsMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
