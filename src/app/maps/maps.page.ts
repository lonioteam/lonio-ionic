import { Component, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MarsMapPage } from '../mars-map/mars-map.page';
import { ShareService } from '../share.service';

@Component({
  selector: 'app-maps',
  templateUrl: 'maps.page.html',
  styleUrls: ['maps.page.scss']
})
export class MapsPage {
  constructor(private elementRef: ElementRef, private modalCtrl: ModalController, private shareService: ShareService) {

  }

  ngAfterViewInit() {
    //this.elementRef.nativeElement.querySelector("#marsImage").addEventListener("touchmove", this.onDrag.bind(this));
  }
  
  onDrag(event) {
    console.log(event);

    let rect = event.target.getBoundingClientRect();
    let x = event.targetTouches[0].pageX - rect.left;

    //let x2 = event.targetTouches[0].pageX - event.target.clientX;
    //let y = event.targetTouches[0].pageY - rect.top;
    //console.log(x2);


    //let touch = event.touches[0] || event.changedTouches[0];
    //let realTarget = document.elementFromPoint(touch.clientX, touch.clientY);
    //let x2 = touch.clientX-touch.screenX;
    //event.offsetY = touch.clientY-realTarget.getBoundingClientRect().y;

    this.elementRef.nativeElement.querySelector("#marsImage").style.backgroundPositionX = x + "px";
    //this.elementRef.nativeElement.querySelector("#marsImage").style.backgroundPositionX = -event.offsetX + "px";
  }

  async clickedGlobe() {
    this.shareService.tapVibrate();
    const map = await this.modalCtrl.create({
      component: MarsMapPage
    });
    map.present();
  }
}
