import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { DeviceFeedback } from '@ionic-native/device-feedback/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';


@Injectable({
  providedIn: 'root'
})
export class ShareService {

  conversationJson: object = {};
  loaderJson: object = {};
  data: object = {};

  messages: any = [{ message: "Connecting...", author: "Terminal", finished: true }];

  // Here will be saved button options which user can choose from
  buttonOptions: any = [];

  loader: any = {text: "", finished: false, start: false};

  beginHidden = false;
  beginHidden2 = false;

  lastChoice: any = {};

  constructor(public http: HttpClient, private storage: Storage, private camera: Camera, private tts: TextToSpeech, private nativeAudio: NativeAudio, public deviceFeedback: DeviceFeedback, public vibration: Vibration) {

    storage.get("data").then((val) => {
      console.log("123");
      if (val != null) {
        this.data = val;
        console.log(val);
        console.log("ima");
      } else {
        this.data = {
          "messages": [{ message: "Connecting...", author: "Terminal", finished: true }],
          "buttonOptions": [],
          "lastChoice": "start"
        };

        console.log("nema");
      }
    });

    //this.storage.set("data", {messages: [{ message: "Connecting...", author: "Terminal", finished: true }], buttonOptions: [], lastChoice: "start"});

    this.http.get("./assets/json/story.json").subscribe(data => {
      this.conversationJson = data;
    });

    this.http.get("./assets/json/loader.json").subscribe(data => {
      this.loaderJson = data;
    });

  }

  say(author, message, choice) {
    this.messages.push({ message: message, author: author, finished: false });
    this.saveToStorage(this.messages, this.buttonOptions, choice.name);
  }

  startLoading() {
    this.loader.text = this.loaderJson["text"];
    //this.loader.text += this.loader.text;
    this.loader.finished = false;
    this.loader.start = true;

    this.messages = this.data["messages"];

    this.nativeAudio.preloadComplex("music", "../assets/music/music.mp3", 1, 1, 0);

  }

  loaderCompleted() {

    this.nativeAudio.loop("music");

    //this.nativeAudio.setVolumeForComplexAsset("music", 0.6);

    this.loader.finished = true;

    this.beginHidden2 = true;

    let data = this.conversationJson["passages"];

    let start = {};

    if (this.data["lastChoice"] == "start") {
      data.forEach(choice => {
        //console.log(choice);
        if (choice.name == "start") {
          start = choice;
        }
      });
    } else {

      console.log(this.data["lastChoice"]);

      data.forEach(choice => {
        if (choice.name == this.data["lastChoice"]) {
          console.log(choice);
          choice.skip = true;
          start = choice;
        }
      });
    }

    this.startChoice(start);

    setTimeout(() => {
      this.tts.speak({
        text: this.messages[0].message,
        locale: 'en-CA',
        rate: 0.95
    }).then(() => {
        console.log("OK");
      }).catch((error) => {
        console.log("Err", error);
      });
    }, 2000);
  }

  startChoice(choice) {
    //console.log(choice.skip);
    if (choice.name != "end" && choice.skip == undefined) {
      
      this.lastChoice = choice;

      console.log(choice);

      choice.text = choice.text.replace(/ *\[[^\]]*]/g, '');
      choice.text = choice.text.slice(0, -1 * (choice.links.length + 1));
      

      let hours:any = new Date().getHours();
      let minutes:any = new Date().getMinutes();

      if(hours < 10) hours = '0' + hours.toString();
      if(minutes < 10) minutes = '0' + minutes.toString();

      choice.text = this.replaceAll(choice.text, '/time/', hours.toString() + ':' + minutes.toString());

      //console.log(choice.text);


      //if (choice.name.substring(0, 11))
      if (choice.name.substring(0, 11) == "TakePicture") {
        this.takePhoto(choice);
      } else {
        this.buttonOptions = choice.links;
        this.say("Robert", choice.text, this.lastChoice);
      }
    } else if (choice.skip) {
      this.buttonOptions = choice.links;
    }
  }

  selectedButton(button) {
    console.log(button);

    this.buttonOptions = [];
    this.say("Me", button.name, this.lastChoice);

    let data = this.conversationJson["passages"];
    let nextChoice = {};
    data.forEach(choice => {
      if (choice.name == button.link) {
        nextChoice = choice;
      }
    });

    this.startChoice(nextChoice);
  }

  startChapter(): any {
    
    this.beginHidden = true;
    this.startLoading();

  
  }

  clickedText(i){
    this.messages[i].finished = true;
  }

  replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, "g"), replace);
  }

  saveToStorage(messages, buttonOptions, lastChoice) {
    this.storage.set("data", { messages: messages, buttonOptions: buttonOptions, lastChoice: lastChoice });
  }

  resetStory() {
    this.data = {
      "messages": [{ message: "Connecting...", author: "Terminal", finished: true }],
      "buttonOptions": [],
      "lastChoice": "start"
    };

    this.storage.set("data", this.data);
  }


  completedWrite(i) {
    this.messages[i].finished = true;

    if (this.messages[i].author == "Me") {
      setTimeout(() => {
        this.tts.speak({
          text: this.messages[i + 1].message,
          locale: 'en-CA',
          rate: 0.9
      }).then(() => {
          console.log("OK");
        }).catch((error) => {
          console.log("Err", error);
        });
      }, 2000);
    }

  }

  canWrite(i) {
    if (i == 0) {
      if (this.messages[0].finished) return false;
      else return true;
    } else {
      if (this.messages[i - 1].finished) {
        if (!this.messages[i].finished) return true;
        else return false;
      } else {
        return false;
      }
    }
  }

  async takePhoto(choice) {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    console.log("Usli smo u sliku");

    //console.log(choice.split("-"));

    //alert("HEllo");

    await this.camera.getPicture(options).then(async (imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // console.log("Bok!")
      // console.log(imageData);
      // console.log("Bok!")
      // let base64Image = 'data:image/jpeg;base64,' + imageData;

      this.getText(imageData, choice);

      // alert(JSON.stringify(imageData));
    }, (err) => {
      // Handle error
      console.log("err", err);
    });
  }

  tapVibrate() {
    this.deviceFeedback.acoustic();
    //this.deviceFeedback.haptic(1);

    this.vibration.vibrate(50);
  }

  getText(file, choice) {
    if (window["MlKitPlugin"]) {
      /* console.log('Get Text');
      window['MlKitPlugin'].getText(file, {},
        (success) => {
          console.log('GET TEXT SUCCESS', success);
          alert("Local " + JSON.stringify(success));
        },
        (error) => {
          console.log('GET TEXT ERROR', error);
          alert("Local " + JSON.stringify(error));
        });

      window['MlKitPlugin'].getTextCloud(file, { language: 'en' },
        (success) => {
          console.log('GET TEXT CLOUD SUCCESS', success);
          alert(JSON.stringify(success));
        },
        (error) => {
          console.log('GET TEXT CLOUD ERROR', error);
          alert(JSON.stringify(error));
        }); */


      window['MlKitPlugin'].getLabel(file, {},
        (success) => {
          //console.log('GET TEXT SUCCESS', success);
          //alert("LABEL " + JSON.stringify(success));
          //return success;

          alert(JSON.stringify(success));

          let labels = [];

          for (let i in success) {
            // i = 1

            labels.push(success[i]["text"].toLowerCase());

          }

          alert(JSON.stringify(labels));

          let neededLabels = choice.name.split("-")[2].split(",");
          console.log(neededLabels);

          let found = false;
          for (let i = 0; i < neededLabels.length; i++) {
            if (labels.indexOf(neededLabels[i]) > -1) {
              found = true;
            }
          }

          if (found) {
            alert("DOBRA SLIKA");
            let nextChoiceName = choice.links[1].name;

            let data = this.conversationJson["passages"];

            let nextChoice: any;

            data.forEach(choice => {
              if (choice.name == nextChoiceName) {
                nextChoice = choice;
              }
            });

            this.startChoice(nextChoice);

            //this.startChapter(choice.)
          } else {
            alert("LOSA SLIKA");

            let nextChoiceName = choice.links[0].link;

            let data = this.conversationJson["passages"];

            let nextChoice: any;

            data.forEach(choice => {
              if (choice.name == nextChoiceName) {
                nextChoice = choice;
              }
            });

            this.startChoice(nextChoice);
          }

        },
        (error) => {
          console.log('GET TEXT ERROR', error);
          alert("LABEL ERROR " + JSON.stringify(error));
        });
    }
  }



}
